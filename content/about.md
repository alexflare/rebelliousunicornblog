---
date: "2019-11-25"
title: "Who is the Rebellious Unicorn?"
---

Sup, I'm Pandita and I'm a software engineer from Venezuela. I'm pretty language agnostic, I currently work with WPF/MVC/XAMARIN/C#. I've played with many languages, thanks to my crazy university teachers (❤️).

I started web development very young, when I was around 11-12 years old. At the time I had one mission and it was to showcase my pixel dolls and drawings. Which I did! I played a lot with table layouts and very simple CSS to make my sites, but they looked good. Fast forward to 2016, I relocated to Mexico and got a job where I'm in a team in charge of developing in a WPF app that deals with shipping logistics, we're currently in a famous pharmacy here in Mexico, check it out if you can! ;3 

I'm a big fan of jokes, meme, pandas, emojis, pink, interior design, news, tech, web/mobile design, drawing a lot and stuffing myself with spicy Mexican food. I'm a lurker and quite shy on the internet, unlike in real life where I can chit chat about the latest celebrity gossip with pretty painted rocks.

⭐⭐⭐

#### **Contact details**

Email: [alexabergna@gmail.com](mailto:alexabergna@gmail.com)

Instagram: [@monsterpandita](https://www.instagram.com/monsterpandita/)

⭐⭐⭐

![alt text](https://i.imgur.com/vJS7kZr.png "a beautiful pandita posing with wings on her back")