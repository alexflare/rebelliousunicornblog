---
title: Arte y desarrollo de software - La unión de dos mundos🌎
date: 2020-04-19T05:16:17.612Z
tags:
  - español
  - thought piece
  - arte
  - software
  - pensamientos
---
Desde que tengo memoria me ha encantado dibujar. A medida que fui explorando el mundo de la internet, cai en un hoyo de conejo llamado "Dolling", un tipo de pixel art. Tristemente, hoy en dia el dolling ha decaido mucho en popularidad y ahora vive en el recuerdo de mucha jovencitas, pero esta comunidad me ayudo a abrir dos puertas: la programacion y el dibujo. 😱

![Una de mis primeras paginitas webs de dolling](/images/uploads/pic1.png)

*Una de mis primeras paginitas webs de dolling.*

Veran, todas queriamos mostrar nuestras dolls en nuestros propios ambientes y estetica, pero tambien queriamos animar a las demas a participar entonces habian muchisimas chicas que en su portal web tenian una seccion de como hacer paginas webs. Te enseñaban lo basico de html y css, como subirlo a un host gratis y en un dia o menos tenias tu propia paginita web. Tambien te enseñaban a hacer efectos con javascript. Es decir, yo de 12 años ya andaba furulando con el lenguaje IN de estas ultimas decadas. 💅

![Esta pagina web sigue siendo una de mis favoritas](/images/uploads/pic2.png)

*Esta pagina web sigue siendo una de mis favoritas.*

Ahora bien, al pasar los años mi hermano decidio magicamente hacer un comic. Un Nuzlocke en especifico. Yo tenia años dibujando, desde que el dolling me abrio esa puerta pero nunca hice un comic apesar que era uno de mis sueños. No perdi la oportunidad de ser la artista para la historia de mi hermano. El comic me ayudo a desarrollar una habilidad de diseño bastante respetable, tanto asi que en mi actual trabajo me han dado trabajos de diseños. No son perfectos pero hago mi mejor esfuerzo.

![Dos paneles de una pagina del comic!](/images/uploads/pic3.png)

*Dos paneles de una pagina del comic!*

Entonces, mi yo pequeña estaba dividida entre dos mundos: uno altamente logico💻 y otro altamente artistico 🎨.

Para ser honesta, pense que el front-end era la union de estos dos mundos, pero viendo el estado actual del mismo... ya no lo veo asi. Intente con el diseño por medio de unos trabajitos freelance y aunque es bastante creativo, a la vez me senti altamente limitada.

Pero comprendi que trabajar entre limites y restricciones es lo que realmente permite que fluya la creatividad. Entendi que en base a mis conocimientos y experiencias, la union entre mis dos mundos es la solucion de problemas. Me di cuenta que me fascina buscar y proponer soluciones. Un problema trae limitaciones y restricciones y crear una solucion que se estruje entre ellas es muy satisfactorio. ✨

Actualmente sigo muchas chicas en nuestra industria que les fascina el arte y la programacion, estoy segura que ellas tendrian una respuesta distinta a la mia sobre la union entre los dos mundos. Mas bien mi experiencia es una mas del monton! Pero no lo se, casi no he hablado del tema con otros devs.

Para ti? cuales son tus mundos 🌍 y como los unes?

**PD: Este blog post es una excusa para mostrarles las paginas webs que hice de pequeña.🦒**

![Chiquilla sonriendo malevolamente](/images/uploads/pic4.png)