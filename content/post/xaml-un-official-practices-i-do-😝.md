---
title: "XAML un-official practices I do \U0001F61D"
date: 2019-12-11T03:17:22.215Z
tags:
  - english
  - XAML
  - WPF
  - tips
---
These aren't official best practices, but more like habits I've come to have since I started to work with XAML almost 4 years ago! I've done lots of experiments in WPF in terms of readability and maintainability and this is what I ended up with as my own little best practices to follow 🎉. Hopefully they can help you as well.

## Giving all elements a meaningful name

This one is a no brainer but, what I want to offer is my formula: 

```
[element name/short hand] + [what it's for / what it shows]
```

For example, if I want to show a label that is a title for a customer info box, I may name my elements like this: 

```
<Grid x:Name="gridCustomerInfoBox">    
    <Label x:Name="lblTitleCustomerInfoBox" Content="Customer Information:" />
</Grid>
```

Yes it's a bit long, but if you were searching for these elements in your code behind, typing "InfoBox" or "Customer" the Intellisense should return all the elements associated with your grid. It's a QoL life thing. Which is why I would suggest naming all of your elements. No matter how small they may be but, this is something you might want to decide along side your team. 😊

## Download an XAML Styler

It's very important to code in XAML to have a code beautifier, mine is set up to run each time I save, it also puts each property on their own line if  their is more than three properties for your element, making your XAML code much more easier to understand and read. I use XAML Styler by Team Xavalon which you can easily find in the marketplace. It also indents your code and overall is the best #fangirling, but try out different tools and see which one suits you and your team best.  

## Separate elements with new lines

I separate elements with new lines, except when they're opening and closing elements, I tend to keep those together. I feel like it helps visualize what is inside an element, when they start and finish. This is a small habit I got from my small time as a web developer. I needed to know how to control all those bootstrap divs! 😆

So, for example: 

```
 <Grid x:Name="gridTestContent" Grid.Row="1">
    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
    </Grid.ColumnDefinitions>

    <Grid.RowDefinitions>
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
    </Grid.RowDefinitions>

    <Label x:Name="lblTest" Content="Panda Test" />
</Grid>
```

As you can see, the first element "Grid.ColumnDefinition" is right next to the "Grid" tag line (I'll just call it like that) and the label element is right before the grid's closing tag. When you have a lot of elements inside a grid, it's much easier to identify the start and ending of it this way. 

## Set style triggers right under the element

Continuing with our grid. If we were to set triggers for it and we have lots of elements, having our style triggers at the end might confuse us and even more if we have grids inside grids. I like to stick them right after the opening tag to avoid this confusion. 

```
<Grid x:Name="gridTestContent" Grid.Row="1">
    <Grid.Style>
        <Style TargetType="Grid">
            <Style.Triggers>
            <!-- triggers go here -->
            </Style.Triggers>
        </Style>
    </Grid.Style>

    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
    </Grid.ColumnDefinitions>

    <Grid.RowDefinitions>
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
    </Grid.RowDefinitions>

    <Label x:Name="lblTest" Content="Panda Test" />
</Grid>
```

## Adding opening and closing tags on really long elements

Grids are the main culprits of this, alongside stackpanels and all those layout components we use. They can end up really long if it's a very complicated screen, so I tend to add opening and closing comments to help me out. There's a plugin for regions in the marketplace, which I found to be extremely useful (so much that I went overboard and added them in places where they really weren't needed. omg.). My main issue with this plugin was that my teammates didn't like it or didn't even knew about it. So they started moving the region comments and it became mayhem. 

So, now I only add normal comments that let me know when something is starting and ending. 

```
<!-- Test Content -->
<Grid x:Name="gridTestContent" Grid.Row="1">
    <Grid.Style>
        <Style TargetType="Grid">
            <Style.Triggers>
            <!-- Triggers go here -->
            </Style.Triggers>
        </Style>
    </Grid.Style>

    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
    </Grid.ColumnDefinitions>

    <Grid.RowDefinitions>
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
    </Grid.RowDefinitions>

    <Label x:Name="lblTest" Content="Panda Test" />
</Grid>
<!-- END: Test Content -->
```

Very simple right? 

## Triggers are life

If you can, try to use triggers for most of your UI behavior, sometimes it can be done, sometimes it can't but do try. 

## I'm Team Grid + StackPanels

I'll admit, I don't use any other layout elements for my screens, I ❤️ GRIDS AND STACKPANELS. With only just grids I've been able to create elastic/fluid screens. I still haven't worked on making a fully responsive one, but it's in my bucket list! Stack panels are pretty much my "I don't want to make a grid/column/row for this", they are a bit finicky to work with but, they've saved my life several times. They'd be your typical tsundere love interest in a manga. Play around with them and fall in love with them just as I did. They're one of my best assets when it comes to laying out everything in the screen especially grids. 

```
<!-- Test Content -->
<Grid x:Name="gridTestContent" Grid.Row="1">
    <Grid.Style>
        <Style TargetType="Grid">
            <Style.Triggers>
            <!-- Triggers go here -->
            </Style.Triggers>
        </Style>
    </Grid.Style>

    <Grid.ColumnDefinitions>
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
        <ColumnDefinition Width="*" />
    </Grid.ColumnDefinitions>

    <Grid.RowDefinitions>
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
        <RowDefinition Height="*" />
    </Grid.RowDefinitions>

    <StackPanel 
        x:Name="spTest"
        Orientation="Horizontal" 
        Grid.Column="1" 
        Grid.Row="1">
            <Label x:Name="lblTest" Content="Panda Test" />
            <Label x:Name="lblTesting" Content="This is test" />
    </StackPanel>
</Grid>
<!-- END: Test Content -->
```

## Create User Controls when you have to

I like user controls, so much that I tend to go a bit crazy with them tho'... they allow you to abstract UIs A LOT. They're way too forgiving and they give a lot of power. My teammates moan when they have to go to a specific screen because I made a user control inception there, I mean you can find an event bubbling up through 4 to 5 levels of user controls type of inception. 

My advice? Use them when they make sense to be used. Do you have a piece of UI that repeats itself multiple times? it doesn't really matter if it's on one screen or several. There I would say to create a user control for it. For example, a header, a footer, a button with an icon, etc. 

# Conclusion

These are just a few little tips/habits/practices that hopefully you might find useful, but I do urge you to try and create your own style and habits. Play around with everything XAML has to offer, find those elements you really enjoy using and enjoy the process or creating UI in WPF 🥰.

Until next time! Bye ~
