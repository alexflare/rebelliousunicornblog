---
title: Dos guías de referencias para Thunkable
date: 2019-12-02T02:18:06.859Z
tags:
  - español
  - thunkable
  - guias
  - referencias
  - technovation
---
![Thunkable beaver ](/images/uploads/thunkable.png "Al infinito y más alla! ")

A principios de este año me toco ser mentora de un grupo de chicas para un programa llamado Technovation, cuyo objetivo principal es animar a las niñas a ser parte del mundo de la programación por medio de la creación de una app que resuelva algún problema en el mundo.

Para ser honesta, me tocó ser mentora ya casi al final de la época de desarrollo. Las pobres no sabían nada de programación, nos quedaban 4 semanas y yo en la mitad de una crisis existencial. Pero bueno, eso es un cuento para otro post. Así que ni corta ni perezosa, me puse manos a la obra y creé dos guías que pudieran usar como referencia a la hora de estar programando su aplicación. 

Así que con la esperanza de que puedan ayudar a algún mentor en el futuro, decidí compartirlos por este espacio. 

[**Thunkable - Referencia I: Una breve introducción al mundo de la programación**](https://www.dropbox.com/s/ft0w1c82vcdsp7i/Thunkable_Referencia.pdf?dl=0)

[**Thunkable - Referencia II: Ejemplos**](https://www.dropbox.com/s/a1k7e146c1el6is/Thunkable_Referencia_II.docx.pdf?dl=0)

Espero que sean de mucha ayuda! 🎉

\--

_PD: No se si mis chicas al final leyeron los documentos_ 😂.
