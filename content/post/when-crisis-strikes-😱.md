---
title: "When crisis strikes...\U0001F631"
date: 2020-01-10T03:01:00.000Z
tags:
  - storytime
  - team
  - teamwork
  - retrospective
  - english
---
![Image result for sipping coffee](https://p0.pikrepo.com/preview/413/783/woman-sipping-coffee.jpg)

One fine morning, after getting myself some coffee and having an amicable talk with a coworker, I stood up from my seat and headed to to the nearby conference table for our sorta boring daily stand-up. When it was my turn, I said what I worked the day before and what I would focus on for the rest of the day. You know, *the usual*. Everyone said their part, we saw our burndown chart and capacities, joked that a few of us were in red and our scrum master (who at any moment was going to fall asleep) asked if anybody had anything for the parking lot. One QA team member decided to ask a a very innocent question and give us a small heads up.

***Then all hell broke loose.***

What the QA team member wanted to let us know was that our most important and high risk feature for one of our most important client had been tested in a temporarily impossible production configuration because we (me) forgot to support the main configuration. One of our product owners started to freak out because we were set to release in less than two weeks. The development and QA teams had absolutely no idea that there was going to be a release with said feature in such short-notice and not only that, the backlog looked NOTHING like we were set out to release.

Our business analyst was royally pissed because they had told the QA team that testing the release candidate in that configuration was madness but they were ignored. Development didn't keep track of the feature and the product owners believed we had finished the feature correctly... in other words, we were all very guilty. So, the team decided to create a strategy and...

**Everything fell on the shoulders of one developer.** After that meeting the entire team was in a state of "what the?" for at least one hour until we decided that the solution was not good enough and slowly but surely began to grab tasks for ourselves and push/ghost other backlogs and bugs. Our business analyst who used to be a QA gave us a list of all the areas we had to test to make sure the app was working correctly while that one lonely developer was trying to implement all the missing backlogs for the feature. Whenever the developer in question got a bug related to the feature and it was something any of us could grab we would do so. I created a document that helped us check our different production configurations for me and other team members, where even the head of QA liked it and decided to use it as a way to show the team's testing progress (gotta brag a bit! I'm quite proud of myself👻). We collaborated in a way we had never done so before. We fixed bugs together, we did cross testing, we pair programmed and deployed quickly for QA to test and we didn't have to work overtime at all.

Sadly, we had to ask for one more week to push the changes, but by the end of that week, everything had been done and tested. The team rested for a couple of days and our scrum master decided that we were now going to do our retrospective.

So, let's dive in into what the team learned.

**Lesson 1: Collaboration and Communication**

Were the key factors that allowed the team to face this crisis. We sat down and talked about our current problems, asked each other questions out loud (we normally chat our work life away), offered help whenever and wherever we could, we would raise flags no matter how small of a bug we found and we sent each other memes. TONS OF MEMES. I believe this dynamic was one of the core reasons that allowed us to sail smoothly across these turbulent holiday waters. We believed in each other as a team, we knew we had each other's support and that we would all be there for one another. Corny? yes, but having the fear that failing means losing one of our clients, was something we really needed.

**Lesson 2: Organization and Re-prioritization**

Our sprint was not planned at all for the release, we believed we were on a normal development cycle. We were doing things that would see the light of day who knows when in 2020. We were relaxed and bored. This crisis was the chile serrano the year needed to make it memorable. A bit of suffering is always a part of a developer's life, so we might as well enjoy it! And so as a team, we decided what was important to fix, what wasn't and whatever questions we had we didn't hesitate in annoying the right people to get an answer. We would fix as many important issues that would come up (there were tons!) almost all on the same day. We would test, merge and push to RC for retesting almost everyday. It was chaos. But, by the end of that extra week we were only fixing really small things.

We didn't really sit down and decide who was doing what, we would just let each other know what we were doing. One of our devs forgot to do so and accidentally fixed a bug that another dev had already fixed. It was a funny moment that later caused a few small mishaps. But in reality, there wasn't one person coordinating us, we were all coordinating ourselves. We were strong independent developers who needed no dev lead 😂.

**Lesson 3: After a storm, asses the wreckage**

The retrospective for this crisis is probably my favorite ever. This was our newly appointed scrum master's first retrospective and it was amazing! everybody had a voice, even our most quietest members. We talked about processes, planning, bugs, dramas, etc. we let it all out pretty much. Although I have to be honest, I had a really hard time thinking about what we did wrong as a team since I was overjoyed with the way things had turned out but, maybe it's because I'm an extremely positive person who sometimes needs to be pulled down to reality. Which is what other team members did.

For starters, developers shouldn't have been doing QA 😅, we should've had a better knowledge of the state of the feature and track it correctly, we needed better requirements, more transparency on release dates, better bug descriptions, clarity and vision, etc. etc.

Based on this feedback, we created a plan for the future. But it was sidetracked because most of us (me included) headed off to their holiday vacation.

But thinking this a bit more, I think as a team we didn't realize the best thing we did:

**We had empathy.**

The team didn't wait for a sprint backlog re-prioritization by our POs and SM, we decided to reorganize ourselves and change our priorities and focus on what was truly at stake. We were there helping each other out and supporting the team members that were starting to feel overwhelmed. We believed in pushing this out because we wanted our users to use this feature that would probably make their lives a bit more easier, we wanted our clients to feel confident and secure and I think, we didn't want to disappoint ourselves, but we didn't work overnight because resting is important.

Having empathy for ourselves is very important.

In conclusion, I believe that moments of crisis will always happen no matter how well prepared your team may be but, it is during those time where people tend to become a bit more closer, because we all have the same goals, same focus and probably the same feeling of desperation 😂.

Anyway, lets get to the real conclusion to this post and that is, the song stuck inside my head while writing this:

![Image result for we're all in this together](https://imgix.bustle.com/rehost/2016/9/13/1c04f17c-f251-44cb-8738-848201198397.jpg?w=970&h=546&fit=crop&crop=faces&auto=format&q=70)

🎶

We're all in this together

Once we know

That we are

We're all stars

And we see that

We're all in this together

And it shows

When we stand

Hand in hand

Make our dreams come true

🎶

Hope you have a nice day 🥰.
