---
title: Story time! El día que mi proyecto desapareció.
date: 2019-12-12T01:12:25.360Z
tags:
  - español
  - diversion
  - anecdotas
  - storytime
---
Esto sucedió hace varios años atrás, era nueva en México y bastante junior. Me contrataron en una empresa cools, me dieron mi primer proyecto grande casi que el mismo día que comencé a trabajar. **_Quemosión_**. Pasé varios meses trabajando en ella, tuve mis altos y bajos pero un día, el glorioso Visual Studio (a veces es malvado ese IDE😤) perdió el mapping del proyecto. Le pedí ayuda a uno de mi seniors y él me dijo, bueno vamos darle click a remappear. 

**Que. gran. error. hermanos.** 

Resulta y acontece, que yo no le había hecho commit a mis cambios, ni al proyecto ni a nada. Más bien, creo que sigue sin estar en un repositorio esa cosa. 

Se borró todo. TO-DO. 

![Así mi cara](/images/uploads/the-selena-gomez.jpg "Selena Gomez crying")

Yo estaba que me arrancaba los pelos! Estábamos cerca de sacar a producción el proyecto, ya lo habíamos enseñado al cliente (y le gustó) y resulta que ahora ya no había proyecto, archivos ni nada en el bendito Visual Studio y en mi computadora. 

Pero, lo bueno es que teníamos un dll del proyecto de un intento de release de hace como una semana. El senior (el cual se sentía bastante culpable 😥) usó una herramienta para descomprimirlo todo y ¡boom! el proyecto ya estaba de regreso.

Lastimosamente, todo, pero todo el proyecto estaba hecho añicos. Tipo cuando estas comiendo chupeta, llegas al chicle y esta todo empegostado del caramelo, algo así. Tardé más arreglando el código para que se viera decente nuevamente que en todo el trajín de recuperarlo (el cual sentí que fue toda una eternidad).

Les comento que ese día fue tan traumante para mí que hasta se me había borrado de la mente y me acordé hace unos días de este evento. 

Lo bueno es que al final, el proyecto se le hizo release y el cliente bien feliz lo sigue usando hasta el sol de hoy 🌞. 

FIN
