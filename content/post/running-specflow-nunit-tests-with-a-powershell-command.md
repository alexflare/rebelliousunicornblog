---
title: How to run Specflow NUnit tests with Powershell Commands
date: 2022-05-04T19:03:31.221Z
tags:
  - specflow
  - nunit
  - powershell
  - command
  - line
---
I'm not good with PowerShell or anything of the sort because I rarely get any time to work with it and I'm a bit tired after work to practice buuuut this time I had a small task to create a "script" (mostly a command) to run Specflow tests with the NUnit runner through PowerShell, so here are the steps I followed after much, newbie moments of trial and error!!

- - -

I'm using Windows so this post will be geared toward Windows users. You'll also need to install Chocolatey like, now, if you don't have it.

Open a PowerShell terminal with admin privileges in your project directory. Install NUnit with the following command `choco install nunit-console-runner`. The call for the runner is `nunit3-console`.

In your PowerShell terminal (I'm using VS Code's PowerShell plugin) use the following command:

`nunit3-console "your project DLL path.dll"`

If you're doing this locally, the project .dll will be in your debug bin. 

The results of the command will show in the terminal and be saved in a TestResult.xml (you'll also get some invisible internal traces, I deleted them but you can decide what to do with them). 

If you need to run a specific test I used the following command:

`nunit3-console "your project DLL path.dll" --where "test =~ NameOfTest"`

or a specific tag:

`nunit3-console "your project DLL path.dll" --where "cat == SpecflowTag"`

**Note 1:** If you're building your project in Visual Studio, you can't use the Specflow Runner and NUnit nugets at the same time, or else you'll get an unexpected task exception when building your project in Visual Studio.

**Note 2:** Since I was working with an empty Specflow project, I needed to add dummy tests with an Assert command that let the NUnit console runner realize that there was a test in the Assembly, it didn't find it without this. Just a small heads up!