---
title: >-
  XML for newbies - This is a generic explanation post with an example that is
  way too specific.
date: 2020-03-10T02:32:33.125Z
tags:
  - english
  - xml
  - newbies
  - tutorial
---
Okay so, this article is mainly for my brother since he recently told me that he didn't understand anything about XML. So, I thought to myself that this would be a great opportunity to write a blog post for newbies like him and that it'll probably be short and sweet! 😊

In order to center the post around his specific needs, I innocently asked him what he needed to learn the most. **This** was his answer:

*"How to divide the tags, then how to call them and order them into a list while also being able to call tags in a dynamic way."*(Disclaimer: I had to fix a few typos there.)

In other words, he needs an XMLserializer class. 😅

Either way, what he asked for is also something I asked when I started. It's probably one of the first things we end up googling when we get started with XML (just a guess). So, I might as well condense it in a very simple newbie friendly blog post right?

Anyway, story time is over! Let's do this.

![Image result for csi yeah meme](https://i.pinimg.com/474x/ff/d4/8b/ffd48bccc811c18dcf914d7dc8b6cdf2--las-vegas-meme.jpg)

Before diving into this subject, it's necessary that you know that a markup language is*"a system for marking or tagging a document that indicates its logical structure (such as paragraphs) and gives instructions for its layout on the page especially for electronic transmission and display"*(Merriam-Webster, 2020). Now that you know what a markup language is, let's begin.

**🔥 The Extensible Markup Language AKA XML**

Created by the World Wide Web Consortium in 1998, XML is a markup language like HTML. Its main purpose is to carry data from one end to another. The system allows it to be both human-readable and machine-readable, facilitating communication. The most important difference between XML and HTML is that XML tags are not predefined. Instead, authors or developers create the tags.

Since these tags are not predefined, programs need to serialize and de-serialize the file for easy manipulation.

TIP: Whenever you're working with API's that return XMLs but don't have good documentation, try to look at the XML response. The answers to your questions might be there. #BasedOnATrueStory

**Also, like crazy thought:**Mokona from Tsubasa Reservoir chronicles might've used something like XML to exchange objects between the CLAMP universes. She was a walking 3D printer that's for sure. But, like let me know your thoughts about this okay? #WildSpec ✨

![Image result for mokona](https://66.media.tumblr.com/2836b48c8e06ee78075d9fba32c14886/tumblr_p7d4jhcsQj1tylu9po1_400.png)

I'm 100% sure that any possibility of employment opportunities from this article just died.

🦄**XML - Parts that conform it**

An XML file contains several parts, described in the following image:

![Doggie XMl](/images/uploads/mimixml.png)

Mimi is cute, isn't she?

* **Elements:** It's everything inside a opening and closing tag, including themselves.
* **Markup:**they're the tags and elements of an XML document.
* **Opening and closing tags:** they're a construct of the XML language.
* **Comments:**they're useful if you need to add explanation or dividers (I'll do a how to make useful comments blog post later ;D).

🍔**XML - Uses**

Personally, I've used it as a way to log information in text files, to communicate with web services, create user interfaces for android apps and for Mexican migration paperwork. Quite versatile to say the least.

![Image result for surprised meme"](https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcRQBuLJRkXIoZsRp1efsENukVS-GXJs381C_h-VJKThF6J3G1gx)

Hold up! What?

Yeah. To be quite frank, I was happily surprised when I saw the Mexican government use XML because I'm super used to the overall "not sure if on purpose" inefficiency of the Venezuelan government. To me it was like WOAH this is like, THE FUTURE (cue all Mexicans reading this face-palming at my statement 🤦). In my defense, I come from a country that has horrible and I mean HORRIBLE, legal paperwork systems. Getting a passport is a quest worthy of a movie. So yeah, kudos to the Mexican government for that.

Besides that, I'm also aware that it's used in video-games for dialogue creation and of course, it's used in web development. A a small note, web development tends to favor JSON (another language used for data transfer), but depending on security needs, you might end up using protocols that only allow XML. 😅

🤓**XML - The incredibly specific example**

Using the dogs XML of the previous example, I'm going to build a small app that'll do the following:

1. Read the XML and populate a list.
2. Edit dogs.
3. Let us write to the XML the changes.

The app should be simple. So, I made a small mockup of it and my idea is that it should look something like this:

![](/images/uploads/mockup.png)

I'll be using WPF, XAML and C# for this.

The first step is to create the UI in XAML. Since this part isn't the most important part of the example, I'll just show you how it looks instead :3

![](/images/uploads/design.png)

While this screenshot is from the designer, it already has our dogs list info! I'll get to how I did it in a bit. Because there's something you need to know first. Also, I decided against the modal for editing because it was turning into a monster that deserves it's own blog post. So, we'll be doing everything in one window.

**What is Serialization?**

Serialization is the act of grabbing an object transforming it, then transferring it and then finally reconstructing it to obtain the exact same object.

~~Sounds like teleportation to me!~~

Back in topic this is how the solution ended up looking like:

![](/images/uploads/solution.png)

We'll be focusing on 3 files only: DogList.xml, Dog.cs and XMLParser.cs

The other files are for design stuff and we currently don't care about that!

So, the first thing I created was my DogList.xml:

![](/images/uploads/doggiexml.png)

\*suddenly realizes that she doesn't know when or why she added the "xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema"" in the XML file\* (don't click on these links pls)

As you can see, it includes 4 dogs to show! But how can our app show our puppers? Well we need to read the file! But before we can even get there we need to specify what our elements are inside an object. Like so:

![](/images/uploads/dogclass.png)

So in our case, the root element will be the Dogs element and we specify it with an attribute (I'm not blogging about this). Afterwards, we need to create a list of the Dog object, said list also needs an attribute that specifies it, which base on our file would be the element, "dogs". Then in the Dog class we also specify each property with it's element. This is simply to let the serializer know where to set the information.

Btw, it's case sensitive so be sure to match the xml file. 😌

I created the XMLParser.cs class to be the one that handles all this reading and writing. I'll be frank I based a lot of my parser on the Microsoft documentation for XML.

Because as an artist you need TO USE REFERENCES. We computer wizards should do the same. Don't reinvent the wheel folks!

I first added the path to the XML file:

![](/images/uploads/path.png)

Then added the reader method:

![](/images/uploads/reader.png)

The try and catch at this point is pure habit. I use it everywhere at work, but it's a nice to have since it helps you catch a lot of errors. Anyway, as you can see, reading an XML file isn't that hard right? Microsoft was nice enough to create the XMLSerializer and the XMLReader to help us out.

All we need to do is use the XMLReader to deserialize the XML file and cast it as the object we desire. The serializer knows that the XML file is a Dogs object, but the output is of type Object. Thus we pull out our nifty little wand and cast a spell on the object and transform it into Dogs. With that object out of the oven, you'll now be able to use it to populate your UI!

Much magic. Very wow.

But let's say you need to update your dogs. Like, Mimi is no longer 3 years old but 4. After doing all the necessary business logic, you want to update your doggie list.

So, I added the saving/updating method:

![](/images/uploads/saver.png)

What it does is pretty much create or get the XML file, and serialize the object back into an XML.

So we go from an object:

![](/images/uploads/object.png)

to an XML:

![](/images/uploads/xmlresult.png)

Ta-da! ✨

I'm missing in the UI the ability to add dogs, but technically you could still use that writer method.

**😎 Conclusions**

XML isn't really that hard \*thinks about searching XML files\*, well just a little bit once you start diving deeper into it. I know you might be asking "But like, Paaaaandita I need to do this every time for every object can't there be something more? *GENERIC*?" Yes, there's a general way of doing this. But, I first need to do a Generics blog post first. Mostly because I like them. c:

Anyway, I hope you like this post since I've been cooking it up since January >:V It's my first time doing something like this so any feedback is more than appreciated.

Project link in Gitlab 🦊💖: <https://gitlab.com/camelopardo/dogsxml>

I highly suggest you look at the project. I have a far easier time explaining myself in code than in writing. Which is why I'm blogging. To get better at explaining myself!

**📚 Sources:**

Markup language. In*The Merriam-Webster.com Dictionary*. Retrieved January 22, 2020, from<https://www.merriam-webster.com/dictionary/markup%20language>

Wikipedia.

Microsoft.

Stack Overflow.