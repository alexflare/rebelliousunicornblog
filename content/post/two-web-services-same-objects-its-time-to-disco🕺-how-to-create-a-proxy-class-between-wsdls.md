---
title: "Two web services + same objects = It's time to disco\U0001F57A [How to create a proxy class between wsdls]"
date: 2019-11-26T04:05:29.234Z
tags:
  - csharp
  - english
  - exe
  - wsdl
---
**TLDR;** 

Create discomap files with the following command:

`disco.exe "file:///[Local path to file]\file.wsdl"`

Use the wsdl.exe to create proxy classes with the following command:

`wsdl.exe /shareTypes [path to discomap file for ws 1] [path to discomap file for ws 2]... /o: [path to proxy class] /n: [namespace of proxy class]`

⭐⭐⭐

**THE JOURNEY TO OBTAIN THE KNOWLEDGE TO CREATE THE TLDR;**

Let's say that you have two or more web services that you need to use in your app. You download them into your project, implement the first one and when you're going to implement the second one, you realize that it has the same objects as the first, and that they need to be filled up exactly the same way but, because they're inside their own wsdl, you would need to do it separately. Effectively making your code \*wet\*.

As you can imagine, I was not having any of this when I found myself facing this exact problem. So I headed into googlelandia and set up a trip to find a solution for this. Ah yes the first thing you'll read is to use the wsdl.exe command "/sharedtype". That's it. That's the solution, so why did it take me a day to make it work? 

Because I'm my team's Jon Snow that's why. So, I decided to create this guide for all of the programming Jon Snows out there. No need to thank me.

_**Yet.**_ 

![We don't pet doggies here](/images/uploads/ghost1.png "We don't pet doggies here")


_we don't pet doggies here_

First things first. You need to know what a discomap is. It's an extension, an xml file that allows your web service to be discoverable and eeeh... I'm going to honest, I didn't and don't care much about what its true life purpose is but, because I'm dumb and didn't know the file:/// extension to create local URI paths, I learned how to create them since I only had the WSDL file. 

So, boot up you visual studio's command prompt and paste whatever code you think you'll need based on that one lonely SO answer:

`wsdl.exe /sharedtypes <path to first webservice wsdl file> <path to second webservice wsdl file>`

Oh yeah, since I'm, like, ME. I headed to the Package Manager Console first 😂

**Tip #1: THE FOLLOWING IS NOT A COMMAND PROMPT:**

![not a command prompt](/images/uploads/lol1.png "not a command prompt")

You can find the actual command prompt here: 

![tools > command line > developer command prompt](/images/uploads/lol2.png "tools > command line > developer command prompt")

And it should look like this:

![the developer command prompt](/images/uploads/lol3.png "the developer command prompt")

Now CD that into your web service folder, maybe it's called "Web References". Right outside there. Nothing bad's going to happen I SWEAR. 

So run that code. 

Bam bam bam, it throws a million errors at 'cha. Because you CD'd wrong. 

`Prompt: "What do you mean WEB(SPACE)REFERENCE?"`

Since you're stressed out, know nothing about CMD except for the command "ipconfig" and "ping" and, need to finish like pront-o, I suggest you CD by folder like this:

`cd /Folder1`

`cd /Folder2`

`...`

It works. _Long live practicality._ 

Now run your code!!

.

.

.

IT. DOESNT. WORK. AGH. 😡

Since you don't know about the file:/// to create a URI for a local file, it'll tell you that it needs a disco. FORREAL. It's telling you that it needs a discomap file, but you only have your WSDL file. What to do? you use the Web Services Discovery Tool (Disco.exe), that's what.  

In the same command prompt, run this code: 

`disco.exe "[Local path to file]\file.wsdl"`

Ah! an error message. Remember that I've been saying that I didn't know how to make local paths into URIs? At this point I had to finally learn that " file:///" exists. Once I did learn it though, I had to think long and hard if I should try using it for the wsdl.exe with it but, I decided to learn how to  create discomap files for wsdls. _Valuable._

The new code will be like this: 

`disco.exe "file:///[Local path to file]\file.wsdl"`

Cool, the discomap files should create itself in the same folder as the WSDL. Now we can use the to create our proxy class in the wsdl.exe tool. 

But before creating it, I'm going to give you two more tips. 

**Tip #2:** **The /o: command** let's you set a filename or a directory name. I for one, had my own proxy class already created so I set the path to it. If you don't set it, it'll create a proxy class probably named "results.cs" in the folder you CD'd in. 

**Tip #3:** **The /n: command** lets you specify the proxy class's namespace. Unless you want it set as global (note: this can cause issues with other web services not part of the proxy) use this. 

The code now looks like this: 

`wsdl.exe /shareTypes [path to discomap file for ws 1] [path to discomap file for ws 2]... /o: [path to proxy class] /n: [namespace of proxy class]`

If you get any schema issues ignore them. I promise you the proxy will work. I wasted like an hour in trying to fix it and it's simply not worth it, especially if it's not your own wsdl. A small note, sometimes it'll create the same object but with sequential numbers like 1, 2, 3... I got this because I went crazy and created a proxy for more than 4 web services. I don't really know how to fix it but just a small warning. 

Anyway, hopefully after going through this small journey with me you'll understand how I was able to create the proxy. It took me a day because there's not a lot of decent explanations on the subject/problem so what I'm explaining here is a summary of everything I went through to get it to work. 

Now go on and enjoy the proxy class 😉

![Jon petting Ghost, the good boy](/images/uploads/ghost2.png "Jon petting Ghost, the good boy")

_We have now acquired the ability to pet doggies_
